import { HamburgerMenuIcon } from "@radix-ui/react-icons";
import PrimaryButton from "../buttons/primary.button";

const HeaderNavigation = (props) => {
  return (
    <div className="w-screen flex justify-between px-4 bg-white">
      <div className="flex py-3 cursor-pointer">
        <div className="pr-4">
          <HamburgerMenuIcon onClick={props.onclick} width={24} height={24} />
        </div>
        <div className="text-sm font-bold ">Youtube</div>
      </div>
      <div className="flex py-3 cursor-pointer">
        <PrimaryButton />
      </div>
    </div>
  );
};
export default HeaderNavigation;
