import {
  ArchiveIcon,
  CubeIcon,
  DiscIcon,
  DiscordLogoIcon,
  EnterIcon,
  HomeIcon,
  LightningBoltIcon,
  MagicWandIcon,
  RocketIcon,
  TimerIcon,
  VideoIcon,
} from "@radix-ui/react-icons";
import PrimaryButton from "../buttons/primary.button";

const SideNavigation = (props) => {

  return (
    <div className={`md:w-2/5 lg:md:w-1/5 w-full ${props.opened?'block':'hidden'} fixed  bg-white z-50 mt-12  md:mt-12`}>
      
      <div className="w-full h-screen  overflow-y-auto">
        <ul className="py-2">
          <li className="flex px-4 py-1 bg-gray-200 hover:bg-gray-200">
            <div className="pr-4">
              <HomeIcon width={24} height={24} />
            </div>
            <div className="text-sm font-bold ">Home</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <MagicWandIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Explore</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <LightningBoltIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Shorts</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <RocketIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Subscriptions</div>
          </li>
        </ul>

        <ul className="border-y py-2">
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <ArchiveIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Library</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <TimerIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">History</div>
          </li>
        </ul>

        <div className="border-y py-2">
          <div className="px-4 py-1 ">
            <div className="pr-4 font-medium text-xs">
              Sign in to like videos, comment and share
            </div>
            <div className="text-xs mt-2">
              <PrimaryButton />
            </div>
          </div>
        </div>
        <ul className="border-y py-2">
          <div className="px-4 py-1 ">
            <div className="pr-4 font-light text-xs">BEST OF YOUTUBE</div>
          </div>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <VideoIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Music</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <DiscIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Sport</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <DiscordLogoIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Gaming</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <CubeIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">News</div>
          </li>
          <li className="flex px-4 py-1 hover:bg-gray-200 cursor-pointer">
            <div className="pr-4">
              <EnterIcon width={24} height={24} />
            </div>
            <div className="text-sm font-medium ">Entertainment</div>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default SideNavigation;
