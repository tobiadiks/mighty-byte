import DynamicButton from "../buttons/dynamic.button";
import { ClockIcon, Half2Icon } from "@radix-ui/react-icons";
const VideoHover = (props) => {
  return (
    <div className={`p-2 shadow-md cursor-pointer bg-white top-auto z-auto`}>
      <div>
        <img
          alt="default"
          className="w-full h-full object-contain"
          src={props.src}
        />
      </div>
      <div className="flex mt-2">
        {/* icon */}
        <div>
          <img
            className="w-6 h-6 object-cover rounded-full"
            alt="default"
            src={require("../../assets/default.jpg")}
          />
        </div>
        {/* details */}
        <div className="px-1">
          <div className=" font-bold text-xs">
            {props.title.slice(0, 16)}...
          </div>
          <div className="text-xs text-gray-400">{props.channelTitle}</div>
          <div className="text-xs text-gray-400">3.0k views . {new Date(props.publishedAt).toDateString()}</div>
        </div>
      </div>
      {/* cta */}
      <div className=" mt-2 w-full">
        <DynamicButton
          title={"WATCH LATER"}
          icon={<ClockIcon height={24} width={24}  />}
        />

        <DynamicButton
          title={"ADD TO QUEUE"}
          icon={<Half2Icon height={24} width={24}  />}
        />
      </div>
    </div>
  );
};

export default VideoHover;
