import { useState } from "react";
import VideoHover from "./videohover.card";

const VideoThumbnail = (props) => {
  const [hidden, setHidden] = useState(true);
  const [showModal, setShowModal] = useState(false);


  const handleEvent = () => {
    
    setShowModal(true);
  };
  const hideModal = () => {
    
    setShowModal(false);
  };

  return (
    <div className="relative cursor-pointer">
      <div
        className="relative p-2 "
        onClick={() => {
          handleEvent();
        }}
        onMouseOut={() => {
          hideModal();
          setHidden(true);
        }}
        onMouseOver={() => {
          handleEvent();
          setHidden(false);
        }}
      >
        <div className="relative">
          <img
            alt="default"
            className="w-full text-xs h-full object-contain"
            src={props.src}
          />
          <div
            className={`${
              hidden ? "hidden" : "block"
            } absolute text-xs bg-black text-white transform -translate-y-4 right-0`}
          >
            keep hovering to play
          </div>
        </div>
        <div className="flex mt-2">
          {/* icon */}
          <div>
            <img
              className="w-6 text-xs h-6 object-cover rounded-full"
              alt="default"
              src={require("../../assets/default.jpg")}
            />
          </div>
          {/* details */}
          <div className="px-1">
            <div className=" font-bold text-xs">
              {props.title.slice(0, 16)}...
            </div>
            <div className="text-xs text-gray-400">{props.channelTitle}</div>
            <div className="text-xs text-gray-400">3.0k views</div>
          </div>
        </div>
      </div>
      <div
        className={`${
          showModal ? "block" : "hidden"
        } absolute transform -translate-x-16 delay-500 -translate-y-6  top-0 w-[30vw] z-50 `}
      >
        <VideoHover
          title={props.title}
          channelTitle={props.channelTitle}
          src={props.src}
          publishedAt={props.publishedAt}
        />
      </div>
    </div>
  );
};

export default VideoThumbnail;
