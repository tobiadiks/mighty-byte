import { AvatarIcon } from "@radix-ui/react-icons";

const PrimaryButton = () => {
  return(<div className="py-1 px-2 ring-1 ring-blue-500 text-sm flex w-fit text-blue-500 hover:bg-blue-500 hover:text-white">
    <AvatarIcon height={24} width={24} className="my-auto mr-1 "/>
    <button className="font-medium">Sign Up</button>
  </div>);
};

export default PrimaryButton;
