const DynamicButton = (props) => {
  return (
    <div className="py-1 my-1 px-2 text-gray-600 text-center text-xs  w-full bg-gray-200  ">
      <div className="mx-auto flex justify-center">
        <div className="my-auto mr-1 ">{props.icon}</div>
        <button className="font-medium">{props.title}</button>
      </div>
    </div>
  );
};

export default DynamicButton;
