
import { useCallback, useEffect, useRef, useState } from "react";

import VideoThumbnail from "./components/cards/videothumbnail.cards";
import HeaderNavigation from "./components/navigations/header.navigation";
import SideNavigation from "./components/navigations/sidebar.navigation";
import useSearch from "./hooks/searchHook";

function App() {
  const [pageNum, setPageNum] = useState(1);
  const { loading, error, list } = useSearch(
    `https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&maxResults=${
      50 * pageNum
    }&q=programming&key=${process.env.REACT_APP_API_KEY}`,
    pageNum
  );

  const observer = useRef(null);
  const handleObserver = useCallback((entries) => {
    const target = entries[0];
    if (target.isIntersecting) {
      setPageNum((prev) => prev + 1);
    }
  }, []);

  useEffect(() => {
    const option = {
      root: null,
      rootMargin: "20px",
      threshold: 0,
    };
    const observerInter = new IntersectionObserver((entries)=>handleObserver(entries), option);
    if (observer.current) observerInter.observe(observer.current);
  }, [handleObserver]);

  const [opened,setOpened]=useState(false)

  return (
    <div className="w-full font-roboto">
      
      <div className="block  z-30 fixed top-0">
        <HeaderNavigation onclick={()=>setOpened(!opened)} />
      </div>
      {/* main container */}
      <div className="flex">
        {/* nav container */}

        <SideNavigation opened={opened}/>

        {/* contains video grid */}
        <div className="w-full right-0 md:w-full min-h-screen   z-20  mt-16  md:mt-16">
          {/* video thumbnail container */}
          <div className="grid grid-cols-1 md:grid-cols-3 lg:grid-cols-4 ">
            {list.map((value) => (
              <VideoThumbnail
                title={value.snippet.title}
                channelTitle={value.snippet.channelTitle}
                src={value.snippet.thumbnails.medium.url}
                publishedAt={value.snippet.publishedAt}
                key={value.id.videoId}
              />
              
            ))
            }
            <div className="z-50 h-4" ref={observer} />
          </div>
          {loading && <div className="text-xs text-center">Loading...</div>}
          { error && (
            <div className="text-xs text-center text-red-500"> Error! Check Internet Or Refresh Browser</div>
          )}
          
        </div>
      </div>
    </div>
  );
}

export default App;
