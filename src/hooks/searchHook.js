import { useState, useEffect, useCallback } from "react";
import axios from "axios";

function useSearch(query, page) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [list, setList] = useState([]);

  const searchQuery = useCallback(async () => {
    try {
      setLoading(true);
      setError(false);
      const res = await axios.get(query);
      
      setList((prev) => [...prev, ...res.data.items]);
      setLoading(false);
    } catch (err) {
      setError(err);
    }
  }, [query]);

  useEffect(() => {
    searchQuery(query);
  }, [query, searchQuery, page]);

  return { loading, error, list };
}

export default useSearch;